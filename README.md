# graylog

Implementaçao de playbook para instalaçao do graylog

# Desenvolvido para executar no sistema operacional Ubuntu 16.04

## Objetivo: 
Fazer a instalaçao de todas as dependencias necessarias para inicializacao do Graylog Server

### ElasticSearch
1. Variaveis: 
* name: Nome do cluster
* path_data: Localizaçao onde os dados serao salvos

2. Exemplo
```
cluster: 
  name: graylog
  path_data: /opt/elasticsearch/data

```

### MongoDB
1. Variaveis
* data_path: Localizacao de onde os dados serao salvos (em caso de container, nao alterar o default - /data/db)
* init_service: Solicita a role para inicializar o serviço (em caso de container com execucao exclusiva do mongodb, o serviço nao deve ser inicializado)
* log_path: Localizacao de onde os logs serao salvos

2. Exemplo
```
mongodb:
  data_path: /opt/mongodb/data
  init_service: 'yes'
  log_path: /opt/mongodb/logs

```

### Requirements
O arquivo ira provisionar as roles necessarias para a correta execucao do playbook, nao deve ser alterado

### Environment
O arquivo ira executar a logica necessaria para a execucao do playbook, nao deve ser alterado

### Hosts
O arquivo define onde o playbook sera executado, atualmente esta configurado para execucao local

### Service
Um serviço que sera executado apos o build da imagem e execucao do container, ele mantera o container em execucao