package main

import (
	"fmt"
	"time"
	"bytes"
	"os"
	"os/exec"
)

func main() {
	for {
			runPlaybook()
			time.Sleep(1 * time.Hour)
	}
}

func runPlaybook() {
	appPath := os.Getenv("REPO_NAME")
	errDir := os.Chdir("/ansible/" + appPath)

	if errDir != nil {
    	panic(errDir)
	}

	cmd := exec.Command("ansible-playbook", "-i", "hosts", "requirements.yml", "environment.yml")
	
	var out bytes.Buffer
	
	cmd.Stdout = &out

	err := cmd.Run()
	
	if err != nil {
		panic(err)
	}

	fmt.Printf("Playbook: %q\n", out.String())
}